import React from "react";
import ReactDOM from "react-dom";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect
} from "react-router-dom";

import Home from './Home';

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <Redirect exact from="/" to="/home" />
                        <Route exact path="/home"><Home /></Route>
                        <Route path="/*">Error 404 page Non trouvé</Route>
                    </Switch>
                </div>
            </Router>
        );
    }
}

const root = document.querySelector('#root');
ReactDOM.render(<App/>, root);
