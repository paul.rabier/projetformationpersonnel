// ./assets/js/components/Home.jsx

import React from 'react';
import img_1 from '../../public/image/Etape1.jpg';
import defaultImg from '../../public/image/Etape1.jpg';
import img_2 from '../../public/image/Etape2.jpg';
import img_3 from '../../public/image/Etape3.jpg';
import ReactDOM from "react-dom";


export default class Home extends React.Component {

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row introTXT roundSquare">
                        <div className="col-md-12 ">
                            <p className="titrePrincipale">
                                CRÉEZ VOTRE PROFIL - INTÉGREZ LA SLP COMMUNITY EN 3 ÉTAPES
                            </p>
                            <p className="textStyle">
                                En tant que consommateur, vous avez des besoins. Pour répondre à ces derniers, vous
                                allez faire des choix individuels qui engendreront une série d’impacts positifs ou
                                négatifs d’un point de vue économique, social et environnemental. Pour améliorer ces
                                choix et donc votre impact, nous vous proposons d’élaborer votre profil et d’intégrer la
                                SL community. Vous pourrez alors devenir consomm’acteur à travers 8 processus
                                fondamentaux :
                            </p>
                        </div>
                    </div>
                    <div className="row littleList">
                        <div className="col-md-12">
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        1. Trouver et acheter des produits durables près de chez vous grâce au
                                        “Répertoire vert”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        2. Trouver des alternatives à vos choix de vie quotidiens grâce à la “Plateforme consom'action”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        3. Effectuer des changements d’habitude et les comparer avec d’autres grâce au programme et à “l’empreinte chromatique”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        4. Produire vous même vos propres fruits et légumes grâce au programme “Grow Your Own”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        5. Mettre en commun vos ressources et bénéficier du soutien de la communauté grâce à la “plateforme d'économie collaborative / sharing economy”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        6. S’amuser, faire du sport, sortir et rencontrer du monde grâce au “programme GES”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        7. Mettre en place un plan de changement et en mesurer immédiatement les bénéfices pour soi et son entourage grâce au programme “Green coaching”
                                    </p>

                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-3">
                                    <img src={defaultImg}/>
                                </div>
                                <div className="col-md-9">
                                    <p className="textlitL">
                                        8. Apprendre à transformer ses choix grâce aux “Ateliers éco”.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row bigList">
                        <div className="col-md-12">
                            <div className="row roundSquare">
                                <div className="col-md-5 left">
                                    <img src={img_1}/>
                                </div>
                                <div className="col-md-7 right">
                                    <p className="titleBigL">test titre</p>
                                    <p className="textBigL">test txt</p>
                                </div>
                            </div>
                            <div className="row roundSquare">
                                <div className="col-md-5 left">
                                    <img src={img_2}/>
                                </div>
                                <div className="col-md-7 right">
                                    <p className="titleBigL">test titre</p>
                                    <p className="textBigL">test txt</p>
                                </div>
                            </div>
                            <div className="row roundSquare">
                                <div className="col-md-5 left">
                                    <img src={img_3}/>
                                </div>
                                <div className="col-md-7 right">
                                    <p className="titleBigL">test titre</p>
                                    <p className="textBigL">test txt</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

//
// const root = document.querySelector('#root');
// ReactDOM.render(
//     <Home/>
//     , root);
