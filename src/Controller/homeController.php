<?php


namespace App\Controller;


use App\Entity\Items;
use App\Entity\Peoples;
use App\Entity\Types;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class homeController extends AbstractController
{
    private $logger;
    private $isDebug;

    /**
     * @Route("/", name="app_accueil")
     */
    public function accueil(EntityManagerInterface $entityManager): Response
    {
        return $this->render('page/accueilpage.html.twig');
    }

    /**
     * @Route("/{reactRouting}", name="reactHome", defaults={"reactRouting": null})
     */
    public function index()
    {
        return $this->render('page/react/index.html.twig');
    }




    /**
     * @Route("/types/new", name="app_newType")
     */
    public function newType(EntityManagerInterface $entityManager)
    {

        $type = new Types();
        $type ->setLabel('watch');

        $type2 = new Types();
        $type2 ->setLabel('accessory');

        $type3 = new Types();
        $type3 ->setLabel('cloak');

        $entityManager->persist($type);
        $entityManager->flush();
        $entityManager->persist($type2);
        $entityManager->flush();
        $entityManager->persist($type3);
        $entityManager->flush();

        return new Response(sprintf(
            'type is create', $type->getId(), $type->getLabel(),
            $type2->getId(), $type2->getLabel(), $type3->getId(), $type3->getLabel()
        ));
    }

    /**
     * @Route("/items/new", name="app_newItems")
     */
    public function newitems(EntityManagerInterface $entityManager)
    {

        $item = new Items();
        $item ->setName('app watch')
            ->setPrice(150)
            ->setIdType(1)
            ->setIdPeoples(1);

        $item2 = new Items();
        $item2 ->setName('alliance')
            ->setPrice(1500)
            ->setIdType(2)
            ->setIdPeoples(1);


        $entityManager->persist($item);
        $entityManager->flush();
        $entityManager->persist($item2);
        $entityManager->flush();


        return new Response(sprintf(
            'item is create',
            $item->getId(), $item->getName(), $item->getPrice(), $item->getIdType(), $item->getIdPeoples(),
            $item2->getId(), $item2->getName(), $item2->getPrice(), $item2->getIdType(), $item2->getIdPeoples()
        ));
    }

    /**
     * @Route("/page/homepage.html.twig", name="app_homepage")
     */
    public function show(): Response
    {
        return $this->render('page/homepage.html.twig');
    }








}