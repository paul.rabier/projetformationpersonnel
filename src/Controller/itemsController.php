<?php


namespace App\Controller;


use App\Entity\Items;
use App\Entity\Peoples;
use App\Entity\Types;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class itemsController extends AbstractController
{
    private $logger;
    private $isDebug;

    /**
     * @Route("/items/add/{{Pid}}", name="app_addItem", methods={"GET","POST"})
     */
    public function addItem($Pid,Request $request, EntityManagerInterface $entityM): Response
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class,['label'=> 'nom', 'attr'=> [ 'class'=>'form-control inputPos']])
            ->add('price', IntegerType::class,['label'=> 'prix', 'attr'=> [ 'class'=>'form-control inputPos']])
            ->add('idType',IntegerType::class,['label'=> 'type', 'attr'=> [ 'class'=>'form-control inputPos']])
//            ->add('idtype', EntityType::class, array(
//                'class' => 'App:Types',
//                'choices' => function (EntityRepository $er) {
//                    return $er->createQueryBuilder('c');
//                }
//            ))
            ->add('submit',SubmitType::class,['label'=> 'Create item', 'attr'=> [ 'class'=>'form-control inputPos']])

            ->getForm();

        $form -> handleRequest($request);
        if ($form -> isSubmitted() && $form -> isValid()){

            $data = $form->getData();

//            $newDate = DateTime::createFromFormat("l dS F Y",$data['dateNais']);
//            $newDate = $newDate->format('d/m/Y');

            $items = new Items();
            $items ->setName($data['name']);
            $items ->setPrice($data['price']);
            $items ->setIdType($data['idType']);
            $items ->setIdPeoples($Pid);
            $entityM ->persist($items);
            $entityM ->flush();

            return $this->redirectToRoute('app_fiche_consultation',['action' => 'consultation','id' => $Pid]);

        }
        return $this->render('page/addFiche.html.twig',[
            'Myform' => $form ->createView()
        ]);

    }


}