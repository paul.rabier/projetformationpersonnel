<?php


namespace App\Controller;


use App\Entity\Items;
use App\Entity\Peoples;
use App\Entity\Types;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class peoplesController extends AbstractController
{
    private $logger;
    private $isDebug;


    /**
     * @Route("/people/new", name="app_newPeoples")
     */
    public function newPeoples(EntityManagerInterface $entityManager)
    {

        $peoples = new Peoples();
        $peoples ->setLastName('dutrou')
            ->setFirstName('Marc')
            ->setEmail("DMarc@lol.bel")
            ->setAdresse('18 rue du pic')
            ->setCodeP('33000')
            ->setTel('Xx.Xx.Xx.Xx.Xx')
            ->setDateNais(new \DateTime('2000-01-01'));

        $entityManager->persist($peoples);
        $entityManager->flush();

        return new Response(sprintf(
            'people is create', $peoples->getId(), $peoples->getFirstName(),
            $peoples->getLastName(), $peoples->getAdresse(), $peoples->getCodeP(),
            $peoples->getEmail(), $peoples->getTel(), $peoples->getDateNais()
        ));
    }

    /**
     * @Route("/peoples/add/", name="app_addPeople", methods={"GET","POST"})
     */
    public function addPeoples(Request $request, EntityManagerInterface $entityM): Response
    {
        $form = $this->createFormBuilder()
            ->add('lastName', TextType::class,['label'=> 'nom','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('firstname', TextType::class,['label'=> 'prenom','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('email',TextType::class,['label'=> 'mail','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('adresse',TextareaType::class,['label'=> 'adresse','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('codeP',TextType::class,['label'=> 'code postale','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('tel',TextType::class,['label'=> 'téléphone','attr'=> [ 'class'=>'form-control inputPos']])
            ->add('dateNais', BirthdayType::class, [
                'placeholder' => 'Select a value',
                'label' => 'date de naissance',
                'attr'=> [ 'class'=>'form-control inputPos']

            ])
            ->add('submit',SubmitType::class,['label'=> 'Create peoples','attr'=> [ 'class'=>'btn btn-outline-secondary inputPos']])
            ->getForm()
        ;

        $form -> handleRequest($request);
        if ($form -> isSubmitted() && $form -> isValid()){

            $data = $form->getData();
            $peoples = new Peoples;
            $peoples ->setLastName($data['lastName']);
            $peoples ->setFirstName($data['firstname']);
            $peoples ->setEmail($data['email']);
            $peoples ->setAdresse($data['adresse']);
            $peoples ->setCodeP($data['codeP']);
            $peoples ->setTel($data['tel']);
//            $peoples ->setDateNais($newDate);
            $peoples ->setDateNais($data['dateNais']);
            $entityM ->persist($peoples);
            $entityM ->flush();

            return $this->redirectToRoute('app_tablePage');

        }


        return $this->render('page/addFiche.html.twig',[
            'Myform' => $form ->createView()
        ]);

    }

    /**
     * @Route("/peoples/table", name="app_tablePage")
     */
    public function showTable( EntityManagerInterface $entityManager): Response
    {
        if ($this->isDebug) {
            $this->logger->info('We are in debug mode!');
        }

        $repository = $entityManager->getRepository(Peoples::class);
        /** @var Peoples|null $peoples */
        $peoples = $repository->findAll();
//        if (!$peoples) {
//            throw $this->createNotFoundException(sprintf('no people found'));
//        }




        return $this->render('page/table.html.twig',[
            'peoples' => $peoples,
        ]);
    }

    /**
     * @Route("/peoples/{{action}}/{{id}}", name="app_fiche_consultation")
     */
    public function showFiche($action, $id, EntityManagerInterface $entityManager): Response
    {
        $errorText = '';
        $repository = $entityManager->getRepository(Peoples::class);
        /** @var Peoples|null $peoples */
        $peoples = $repository->find($id);
//        if (!$peoples) {
//            throw $this->createNotFoundException(sprintf('no people found'));
//        }

        $repository = $entityManager->getRepository(Items::class);
        /** @var Items|null $items */
        $items = $repository->findBy(['idPeoples' => $id]);
        if (!$items) {
            $errorText = 'no items found';
        }

        return $this->render('page/consultFiche.html.twig',[
            'errorText' => $errorText,
            'action' => $action,
            'peoples' => $peoples,
            'items' => $items
        ]);
    }
    /**
     * @Route("/peoples/delete/{{id}", name="app_deletePeople")
     */
    public function deletePeoples($id, EntityManagerInterface $entityManager): Response
    {

        $repository = $entityManager->getRepository(Peoples::class);
        /** @var Peoples|null $people */
        $people = $repository->find($id);

        $entityManager->remove($people);
        $entityManager->flush();

        $repository = $entityManager->getRepository(Peoples::class);
        /** @var Peoples|null $peoples */
        $peoples = $repository->findAll();

        return $this->redirectToRoute('app_tablePage');
    }

}